require('../config');
const { database, passport } = require('../utils/index');
const passportMiddleware = require('passport');

const User = require('./app/users/models/User');
const UserSections = require('./app/users/models/UserSections');
const Course = require('./app/courses/models/Course');
const Class = require('./app/classes/models/Class');
const Section = require('./app/sections/models/Section');
const Instructor = require('./app/instructors/models/Instructor');
const Test = require('./app/activities/models/Test');
const TestStudent = require('./app/activities/models/TestStudent');
const Report = require('./app/reports/models/Report');
const Notification = require('./app/notifications/models/Notification');
const Story = require('./app/stories/models/Story');
const StoryStudent = require('./app/stories/models/StoryStudent');

//* START SECTION - TESTS
Section.hasMany(Test, { as: 'tests', foreignKey: 'sectionId', onDelete: 'cascade' });
Test.belongsTo(Section, { as: 'section', foreignKey: 'sectionId' });
//* END SECTION - TESTS

//* START COURSE - TESTS
Course.hasMany(Test, { as: 'tests', foreignKey: 'courseId', onDelete: 'cascade' });
Test.belongsTo(Course, { as: 'course', foreignKey: 'courseId' });
//* END COURSE - TESTS

//* START USERS - TESTS
User.belongsToMany(Test, {
	through: { model: 'TestStudent' },
	as: 'testStudents',
	foreignKey: 'userId',
	onDelete: 'cascade',
});
Test.belongsToMany(User, {
	through: { model: 'TestStudent' },
	as: 'testStudents',
	foreignKey: 'testId',
	onDelete: 'cascade',
});
TestStudent.belongsTo(User, { as: 'user', foreignKey: 'userId' });
TestStudent.belongsTo(Test, { as: 'test', foreignKey: 'testId' });
//* END USERS - TESTS

//* START USERS - STORIES
User.belongsToMany(Story, {
	through: { model: 'StoryStudent' },
	as: 'storyUsers',
	foreignKey: 'userId',
	onDelete: 'cascade',
});
Story.belongsToMany(User, {
	through: { model: 'StoryStudent' },
	as: 'storyUsers',
	foreignKey: 'storyId',
	onDelete: 'cascade',
});
StoryStudent.belongsTo(User, { as: 'user', foreignKey: 'userId' });
StoryStudent.belongsTo(Story, { as: 'story', foreignKey: 'storyId' });
//* END USERS - STORIES

//* START COURSE - SECTIONS
Course.hasMany(Section, { as: 'sections', foreignKey: 'courseId', onDelete: 'cascade' });
Section.belongsTo(Course, { as: 'course', foreignKey: 'courseId' });
//* END COURSE - SECTIONS

//* START USERS - SECTIONS
User.belongsToMany(Section, {
	through: { model: 'UserSections' },
	as: 'section',
	foreignKey: 'userId',
	onDelete: 'cascade',
});
Section.belongsToMany(User, {
	through: { model: 'UserSections' },
	as: 'section',
	foreignKey: 'sectionId',
	onDelete: 'cascade',
});
UserSections.belongsTo(User, { as: 'user', foreignKey: 'userId' });
UserSections.belongsTo(Section, { as: 'section', foreignKey: 'sectionId' });
//* END USERS - SECTIONS

//* START CLASS - COURSES
Class.hasMany(Course, { as: 'courses', foreignKey: 'classId', onDelete: 'cascade' });
Course.belongsTo(Class, { as: 'class', foreignKey: 'classId' });
//* END CLASS - COURSES

//* START INSTRUCTOR - COURSES
Instructor.hasMany(Course, { as: 'courses', foreignKey: 'instructorId', onDelete: 'cascade' });
Course.belongsTo(Instructor, { as: 'instructor', foreignKey: 'instructorId' });
//* END INSTRUCTOR - COURSES

//* START USER - REPORTS
User.hasMany(Report, { as: 'reports', foreignKey: 'userId', onDelete: 'cascade' });
Report.belongsTo(User, { foreignKey: 'userId' });
//* END USER - REPORTS

//* START USER - NOTIFICATIONS
User.hasMany(Notification, { as: 'notifications', foreignKey: 'userId', onDelete: 'cascade' });
Notification.belongsTo(User, { foreignKey: 'userId' });
//* END USER - NOTIFICATIONS

//* START USER - CLASS
Class.hasMany(User, {
	as: 'users',
	foreignKey: {
		allowNull: true,
		field: 'classId',
		name: 'classId',
		defaultValue: null,
	},
	onDelete: 'SET NULL',
});
User.belongsTo(Class, {
	as: 'class',
	foreignKey: {
		allowNull: true,
		field: 'classId',
		name: 'classId',
		defaultValue: null,
	},
});
//* END USER - CLASS

database.sync({ alter: true }).then(() => {
	const config = require('dotenv').config;
	config();
	const express = require('express');
	const path = require('path');
	const app = express();
	const http = require('http');
	const server = http.createServer(app);
	app.set('port', process.env.PORT);

	app.all('*', function (req, res, next) {
		res.header('Access-Control-Allow-Origin', 'http://localhost:3002');
		res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
		res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
		res.header('Access-Control-Allow-Credentials', true);
		if ('OPTIONS' == req.method) {
			res.sendStatus(200);
		} else {
			next();
		}
	});

	app.get('/test', (req, res) => res.status(200).json({ msg: 'welcome' }));

	app.use('/uploads', express.static(path.join('uploads')));

	app.use(express.urlencoded({ extended: false }));
	app.use(express.json({ limit: '50mb' }));
	app.use(express.text({ limit: '50mb' }));

	passportMiddleware.initialize();

	app.use('/api/public', require('./app/publicRouter'));

	app.use(passport.authenticate.authenticate);

	app.use('/api/private', require('./app/router'));

	server.listen(process.env.PORT, () => {
		console.info(`Server is listening on port ${process.env.PORT}`);
	});
});
