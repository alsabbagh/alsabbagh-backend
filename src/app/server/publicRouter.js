const router = require('express').Router();

const { httpStatus } = require('../../../utils');
const { appVersion } = require('../../../utils/common/constants');

/*************************
 * @Router /api/Server *
 *************************/
router.get('/', (_, res) => {
	res.status(httpStatus.OK).send({
		data: appVersion,
	});
});

module.exports = router;
