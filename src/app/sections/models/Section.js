const { DataTypes } = require('sequelize');

const { gender } = require('./enum.json');
const { database } = require('../../../../utils/index');

const Section = database.define('Section', {
	id: {
		type: DataTypes.UUID,
		primaryKey: true,
		allowNull: false,
		defaultValue: DataTypes.UUIDV4,
	},
	title: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	description: {
		type: DataTypes.TEXT,
		defaultValue: '',
	},
	isActive: {
		type: DataTypes.BOOLEAN,
		defaultValue: true,
	},
	maxQuantity: {
		type: DataTypes.INTEGER,
		defaultValue: 0,
	},
	type: {
		type: DataTypes.ENUM,
		values: gender,
		defaultValue: gender[0],
	},
});

module.exports = Section;
