const { httpStatus } = require('../../../utils/index');
const { Op } = require('sequelize');

const SectionModel = require('./models/Section');
const CourseModel = require('../courses/models/Course');
const ClassModel = require('../classes/models/Class');
const UserModel = require('../users/models/User');
const UserSections = require('../users/models/UserSections');
const { gender } = require('./models/enum.json');

class Section {
	constructor(data) {
		this.title = data.title;
		this.courseId = data.courseId;
		this.description = data.description;
		this.maxQuantity = data.maxQuantity;
		this.isActive = data.isActive;
		this.type = data.type;
	}

	async add() {
		try {
			const result = await SectionModel.create(this);
			return {
				data: result,
				code: httpStatus.CREATED,
			};
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	async update(id) {
		try {
			const result = await SectionModel.update(this, {
				where: {
					id: id,
				},
			});
			if (result[0] == 1) {
				return {
					data: 'updated',
					code: httpStatus.UPDATED,
				};
			} else {
				return {
					data: 'something wrong happened',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async delete(id) {
		try {
			const result = await SectionModel.destroy({
				where: {
					id: id,
				},
			});
			if (result === 1) {
				return {
					data: 'deleted',
					code: httpStatus.DELETED,
				};
			} else {
				return {
					data: 'something wrong happened',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async get(courseId = '', userId) {
		try {
			const user = await UserModel.findByPk(userId, { attributes: ['type', 'gender', 'classId'] });

			let where = {};

			if (user && user.type === 'student') {
				const sections = await UserSections.findAll({
					where: {
						userId,
					},
					attributes: ['sectionId'],
				});

				const sectionIds = sections.map((section) => section.sectionId);

				const courses = await SectionModel.findAll({
					where: {
						id: { [Op.in]: sectionIds },
					},
					attributes: ['courseId'],
				});

				const courseIds = courses.map((course) => course.courseId);

				const selectedType = gender.find((_gender) => _gender === user.gender);

				const types = [selectedType, gender[2]];

				where = {
					isActive: true,
					courseId: { [Op.notIn]: courseIds },
					type: {
						[Op.in]: types,
					},
				};
			}

			const result = await SectionModel.findAll({
				where,
				include: {
					model: CourseModel,
					as: 'course',
					where: {
						id: { [Op.like]: `%${courseId}%` },
					},
					include: {
						model: ClassModel,
						as: 'class',
						where: {
							id: { [Op.like]: `%${user.classId ? user.classId : ''}%` },
						},
					},
					attributes: {
						exclude: ['createdAt', 'instructorId', 'updatedAt'],
					},
					distinct: true,
				},
			});

			return {
				data: result,
				code: httpStatus.OK,
			};
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}
}

module.exports = Section;
