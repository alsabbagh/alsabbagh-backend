const Class = require('./service');

module.exports = {
	add: async (req, res) => {
		const data = req.body;
		const result = await new Class(data).add();
		res.status(result.code).send({
			data: result.data,
			token: result.token,
		});
	},

	update: async (req, res) => {
		const data = req.body;
		const classId = req.body.id;
		const result = await new Class(data).update(classId);
		res.status(result.code).send({
			data: result.data,
		});
	},

	deleteClass: async (req, res) => {
		const id = req.params.id;
		const result = await Class.delete(id);
		res.status(result.code).send({
			data: result.data,
		});
	},

	getAll: async (req, res) => {
		const id = req.user.id;
		const result = await Class.get(id);
		res.status(result.code).send({
			data: result.data,
		});
	},
};
