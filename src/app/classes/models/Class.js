const { DataTypes } = require('sequelize');
const { database } = require('../../../../utils/index');

const Class = database.define('Class', {
	id: {
		type: DataTypes.UUID,
		primaryKey: true,
		allowNull: false,
		defaultValue: DataTypes.UUIDV4,
	},
	title: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	isActive: {
		type: DataTypes.BOOLEAN,
		defaultValue: true,
	},
});

module.exports = Class;
