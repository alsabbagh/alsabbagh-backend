const handler = require('./handler');
const router = require('express').Router();
/****************************
 * @Router /public/user *
 ****************************/

router.get('/', handler.getAll);

module.exports = router;
