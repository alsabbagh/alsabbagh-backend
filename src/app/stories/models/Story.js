const { DataTypes } = require('sequelize');
const { database } = require('../../../../utils/index');

const Story = database.define('Story', {
	id: {
		type: DataTypes.UUID,
		primaryKey: true,
		allowNull: false,
		defaultValue: DataTypes.UUIDV4,
	},
	post: {
		type: DataTypes.TEXT,
		allowNull: false,
		defaultValue: '',
	},
	image: {
		type: DataTypes.STRING,
		defaultValue: '',
	},
	isVisible: {
		type: DataTypes.BOOLEAN,
		defaultValue: true,
	},
});

module.exports = Story;
