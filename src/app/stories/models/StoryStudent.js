const { DataTypes } = require('sequelize');
const { database } = require('../../../../utils/index');

const StoryStudent = database.define('StoryStudent', {
	id: {
		type: DataTypes.UUID,
		primaryKey: true,
		allowNull: false,
		defaultValue: DataTypes.UUIDV4,
	},
});

module.exports = StoryStudent;
