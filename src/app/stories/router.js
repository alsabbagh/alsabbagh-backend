const router = require('express').Router();
const { add, deleteStory, getAll, createUserStory, getWatches } = require('./handler');

const { fileUploader } = require('../../../utils');

/*************************
 * @Router /api/Stories *
 *************************/

router.post('/', fileUploader.upload.single('image'), add);

router.delete('/:id', deleteStory);

router.get('/', getAll);

router.get('/user-story', createUserStory);

router.get('/watches/:id', getWatches);

module.exports = router;
