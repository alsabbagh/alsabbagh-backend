const { unlinkSync } = require('fs');
const { Op } = require('sequelize');
const StoryModel = require('./models/Story');
const StoryStudentModel = require('./models/StoryStudent');
const UserModel = require('../users/models/User');

const { httpStatus } = require('../../../utils/index');
const { formateTime, formatDateTime } = require('../../../utils/common/functions');

class Story {
	constructor(data) {
		this.post = data.post;
		this.image = data.image;
		this.isVisible = data.isVisible === false ? data.isVisible : true;
	}

	async add() {
		try {
			const result = await StoryModel.create(this);
			return {
				data: result,
				code: httpStatus.CREATED,
			};
		} catch (error) {
			console.error(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async delete(id) {
		try {
			const record = await StoryModel.findByPk(id);

			if (record.image) {
				unlinkSync(`${record.image.slice(1)}`, function (err) {
					if (err && err.code == 'ENOENT') {
						console.info("File doesn't exist, won't remove it.");
					} else if (err) {
						console.error('Error occurred while trying to remove file');
					} else {
						console.info(`removed`);
					}
				});
			}

			const result = await StoryModel.destroy({
				where: {
					id,
				},
			});

			if (result === 1) {
				return {
					data: 'deleted',
					code: httpStatus.DELETED,
				};
			} else {
				return {
					data: 'something wrong happened',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async get(id, limit, skip) {
		try {
			let user;

			if (id) user = await UserModel.findByPk(id, { attributes: ['type'] });

			const where =
				user && user.type === 'student'
					? {
							isVisible: true,
					  }
					: {};

			const result = await StoryModel.findAndCountAll({
				where,
				limit,
				offset: skip,
			});

			return {
				data: result,
				code: httpStatus.OK,
			};
		} catch (error) {
			console.error(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	async update(ids) {
		try {
			const result = await StoryModel.update(this, {
				where: {
					id: ids,
				},
			});

			if (result[0] == 1) {
				return {
					data: 'updated',
					code: httpStatus.UPDATED,
				};
			} else {
				return {
					data: 'something wrong happened',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}
}

class UserStory {
	static async create(userId) {
		try {
			const stories = await StoryModel.findAndCountAll({
				where: {
					isVisible: true,
				},
			});

			const storyIds = stories.rows.map((story) => story.id);

			const res = await StoryStudentModel.findAndCountAll({
				where: {
					userId,
					storyId: { [Op.in]: storyIds },
				},
			});

			if (res.count === storyIds.length)
				return {
					data: 'done',
					code: httpStatus.OK,
				};

			const results = storyIds.filter(
				(storyId1) => !res.rows.some(({ storyId: storyId2 }) => storyId1 === storyId2)
			);

			const records = results.map((story) => ({
				storyId: story,
				userId,
			}));

			await StoryStudentModel.bulkCreate(records);

			return {
				data: 'done',
				code: httpStatus.CREATED,
			};
		} catch (error) {
			console.log(error);

			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async getWatches(storyId, limit, skip) {
		try {
			const result = await StoryStudentModel.findAndCountAll({
				where: {
					storyId,
				},
				include: {
					model: UserModel,
					as: 'user',
					attributes: ['name'],
				},
				limit,
				offset: skip,
			});

			return {
				data: result,
				code: httpStatus.OK,
			};
		} catch (error) {
			console.log(error);

			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}
}

module.exports = { Story, UserStory };
