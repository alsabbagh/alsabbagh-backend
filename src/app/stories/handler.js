const schedule = require('node-schedule');

const { Story, UserStory } = require('./service');
const { constants } = require('../../../utils/index');

module.exports = {
	add: async (req, res) => {
		const data = {
			image: req.file ? `/${req.file.destination}/${req.file.filename}` : '',
			post: req.body.post,
		};

		const result = await new Story(data).add();

		res.status(result.code).send({
			data: result.data,
		});
	},

	deleteStory: async (req, res) => {
		const id = req.params.id;
		const result = await Story.delete(id);
		res.status(result.code).send({
			data: result.data,
		});
	},

	update: async () => {
		// const
	},

	getAll: async (req, res) => {
		const { limit } = constants;
		const pageNumber = req.query.pageNumber - 1 || 0;
		const skip = pageNumber * limit;

		const id = req.params.id || req.user.id || '';
		const result = await Story.get(id, limit, skip);
		res.status(result.code).send({
			data: result.data,
		});
	},

	getWatches: async (req, res) => {
		const storyId = req.params.id;
		const { limit } = constants;
		const pageNumber = req.query.pageNumber - 1 || 0;
		const skip = pageNumber * limit;

		const result = await UserStory.getWatches(storyId, limit, skip);
		res.status(result.code).send({
			data: result.data,
		});
	},

	createUserStory: async (req, res) => {
		const id = req.user.id;

		const result = await UserStory.create(id);
		res.status(result.code).send({
			data: result.data,
		});
	},
};

async function checkStories() {
	const stories = await Story.get();

	const updatedStories = stories.data.rows.map((story) => {
		const hours = Math.abs(new Date() - new Date(story.createdAt)) / 36e5;

		if (hours >= 24) {
			return story.id;
		}
	});

	if (updatedStories.length > 0 && updatedStories[0] !== undefined)
		await new Story({ isVisible: false }).update(updatedStories);
}

schedule.scheduleJob('0 */1 * * * *', checkStories);
