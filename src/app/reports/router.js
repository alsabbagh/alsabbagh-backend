const handler = require('./handler');
const router = require('express').Router();

/*************************
 * @Router /api/Report *
 *************************/

router.post('/add', handler.add);

router.put('/update', handler.update);

router.delete('/delete/:id', handler.deleteReport);

router.delete('/delete-many/:ids', handler.deleteMany);

router.get('/', handler.getAll);

router.get('/getById', handler.getById);

module.exports = router;
