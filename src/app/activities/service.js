const { httpStatus } = require('../../../utils/index');
const { Op } = require('sequelize');

const TestModel = require('./models/Test');
const User = require('../users/models/User');
const UserSections = require('../users/models/UserSections');
const Course = require('../courses/models/Course');
const Section = require('../sections/models/Section');
const TestStudentsModel = require('../activities/models/TestStudent');

class Test {
	constructor(data) {
		this.title = data.title;
		this.date = data.date;
		this.sectionId = data.sectionId;
		this.courseId = data.courseId;
		this.mark = data.mark;
	}

	async add() {
		try {
			const res = await TestModel.create(this);
			const users = await UserSections.findAll({
				where: {
					sectionId: this.sectionId,
				},
				attributes: ['userId'],
			});
			const rows = users.map((user) => {
				return {
					userId: user.userId,
					testId: res.id,
				};
			});
			await TestStudentsModel.bulkCreate(rows);
			return {
				notification: {
					title: this.title,
					body: 'تم إضافة اختبار جديد',
					sectionId: this.sectionId,
				},
				users,
				code: httpStatus.CREATED,
			};
			// const result = await TestModel.create(this);
			// const result1 = await EnrollmentStudentModel.findAll({
			// 	where: {
			// 		courseId: courseId,
			// 	},
			// 	attributes: ['userId'],
			// });
			// const course = await Course.findOne({
			// 	where: {
			// 		id: courseId,
			// 	},
			// 	attributes: ['title'],
			// });
			// let data = [];
			// let idS = [];
			// for (let i of result1) {
			// 	let isFound = false;
			// 	for (let j of idS) {
			// 		if (i.userId === j) {
			// 			isFound = true;
			// 			break;
			// 		}
			// 	}
			// 	if (!isFound) {
			// 		idS.push(i.userId);
			// 		data.push(i);
			// 	}
			// }
			// let rows = [];
			// for (let i of data) {
			// 	let row = {
			// 		testId: result.id,
			// 		userId: i.userId,
			// 	};
			// 	rows.push(row);
			// }
			// await TestStudentsModel.bulkCreate(rows);
			// const date = result.date.toLocaleString().substring(0, 9);
			// const notification = {
			// 	title: result.title,
			// 	body: course.title + '\n' + date,
			// };
			// return {
			// 	data: notification,
			// 	data2: idS,
			// 	code: httpStatus.CREATED,
			// };
		} catch (error) {
			console.log(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	async update(id) {
		try {
			const result = await TestModel.update(this, {
				where: {
					id: id,
				},
			});
			if (result[0] == 1) {
				return {
					data: 'updated successfully',
					code: httpStatus.UPDATED,
				};
			} else {
				return {
					data: 'something wrong happened!',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async delete(id) {
		try {
			const result = await TestModel.destroy({
				where: {
					id: id,
				},
			});
			if (result == 1) {
				return {
					data: 'deleted successfully',
					code: httpStatus.OK,
				};
			} else {
				return {
					data: 'something wrong happened!',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			console.log(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async getAll(course, section, title) {
		try {
			const result = await TestModel.findAll({
				limit: 60,
				where: {
					courseId: { [Op.like]: `%${course}%` },
					sectionId: { [Op.like]: `%${section}%` },
					title: { [Op.like]: `%${title}%` },
				},
				include: [
					{
						model: Section,
						as: 'section',
					},
					{
						model: Course,
						as: 'course',
					},
				],
				order: [['date', 'DESC']],
			});
			return {
				data: result,
				code: httpStatus.OK,
			};
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async getStudentMarks(id, startDate, endDate, sectionId) {
		const start_date = new Date(startDate);
		const end_date = new Date(endDate);
		try {
			const res = await TestStudentsModel.findAll({
				where: {
					userId: { [Op.like]: `%${id}%` },
				},
				attributes: ['mark'],
				include: {
					model: TestModel,
					as: 'test',
					where: {
						date: {
							[Op.between]: [start_date, end_date],
						},
					},
					attributes: ['title', 'date', 'mark'],
					include: [
						{
							model: Section,
							as: 'section',
							where: {
								id: { [Op.like]: `%${sectionId}%` },
							},
							attributes: ['title'],
						},
						{
							model: Course,
							as: 'course',
							attributes: ['title'],
						},
					],
				},
				order: [[{ model: TestModel, as: 'test' }, { model: Course, as: 'course' }, 'title', 'ASC']],
			});
			return {
				code: httpStatus.OK,
				data: res,
			};
		} catch (error) {
			console.log(error.message);
			return {
				code: httpStatus.BAD_REQUEST,
				data: error.message,
			};
		}
	}
}

class TestStudents {
	constructor(data) {
		this.mark = data.mark;
	}

	static async getAll(testId, name) {
		try {
			const res = await TestStudentsModel.findAll({
				where: {
					testId: { [Op.like]: `%${testId}%` },
				},
				include: {
					model: User,
					as: 'user',
					attributes: ['id', 'name', 'mobilePhone'],
					where: {
						name: { [Op.like]: `%${name}%` },
					},
					order: [['id', 'ASC']],
				},
				attributes: ['id', 'mark'],
			});

			return {
				code: httpStatus.OK,
				data: res,
			};
		} catch (error) {
			console.log(error.message);
			return {
				code: httpStatus.BAD_REQUEST,
				data: error.message,
			};
		}
	}

	static async removeTestUser(id) {
		try {
			const result = await TestStudentsModel.destroy({
				where: {
					id,
				},
			});
			if (result == 1) {
				return {
					data: 'deleted successfully',
					code: httpStatus.DELETED,
				};
			} else {
				return {
					data: 'something wrong happened!',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			console.log(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async update(marks, testId, sectionId) {
		try {
			const users = [];
			await Promise.all(
				marks.map((mark) => {
					users.push({
						userId: mark.userId,
					});
					return TestStudentsModel.update(
						{
							mark: mark.mark,
						},
						{
							where: {
								id: mark.id,
							},
						}
					);
				})
			);
			console.log(testId);
			const test = await TestModel.findByPk(testId, {
				attributes: ['title'],
			});
			console.log(test);
			return {
				notification: {
					title: test.title,
					body: ` تم إصدار علامة الاختبار`,
					sectionId,
				},
				users,
				code: httpStatus.UPDATED,
			};
		} catch (error) {
			console.log(error.message);
			return {
				code: httpStatus.BAD_REQUEST,
				data: error.message,
			};
		}
	}
}

module.exports = {
	Test,
	TestStudents,
};
