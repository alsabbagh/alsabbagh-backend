const { Test, TestStudents } = require('./service');
const { add } = require('../notifications/handler');
const { httpStatus } = require('../../../utils');

module.exports = {
	addTest: async (req, res) => {
		const data = req.body;
		const result = await new Test(data).add();
		if (result.code === httpStatus.CREATED) {
			const response = await add(result.notification, result.users);
			res.status(result.code).send({
				data: response,
			});
			return;
		}
		res.status(result.code).send({
			data: result.data,
		});
	},

	updateTest: async (req, res) => {
		const data = req.body;
		const id = req.body.id;
		const result = await new Test(data).update(id);
		res.status(result.code).send({
			data: result.data,
		});
	},

	deleteTest: async (req, res) => {
		const testId = req.params.testId;
		const result = await Test.delete(testId);
		res.status(result.code).send({
			data: result.data,
		});
	},

	getCourseTests: async (req, res) => {
		const id = req.params.id;
		const result = await Test.getCourseTests(id);
		res.status(result.code).send({
			data: result.data,
		});
	},

	getCourseStudentTests: async (req, res) => {
		const userId = req.params.userId || req.user.id;
		const courseId = req.params.courseId;
		const result = await Test.getCourseStudentTests(userId, courseId);
		res.status(result.code).send({
			data: result.data,
		});
	},

	getAllTests: async (req, res) => {
		const { course, section, title } = req.query;
		const result = await Test.getAll(course, section, title);
		res.status(result.code).send({
			data: result.data,
		});
	},

	getMarks: async (req, res) => {
		const { name, testId } = req.query;
		const result = await TestStudents.getAll(testId, name);
		res.status(result.code).send({
			data: result.data,
		});
	},

	//! remove mark
	removeTestUser: async (req, res) => {
		const id = req.params.id;
		const result = await TestStudents.removeTestUser(id);
		res.status(result.code).send({
			data: result.data,
		});
	},

	updateMark: async (req, res) => {
		const marks = req.body.marks;
		const testId = req.body.testId;
		const sectionId = req.body.sectionId;
		const result = await TestStudents.update(marks, testId, sectionId);
		if (result.code === httpStatus.UPDATED) {
			await add(result.notification, result.users);
			res.status(result.code).send({
				data: 'done',
			});
			return;
		}
		res.status(result.code).send({
			data: result.data,
		});
	},

	getUserMarks: async (req, res) => {
		let { id, startDate, endDate, sectionId } = req.query;
		if (!id) id = req.user.id;
		if (!sectionId) sectionId = '';
		if (!startDate || !endDate) {
			startDate = '8/8/2020';
			endDate = '8/8/2025';
		}
		const result = await Test.getStudentMarks(id, startDate, endDate, sectionId);
		res.status(result.code).send({
			data: result.data,
		});
	},
};
