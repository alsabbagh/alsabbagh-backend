const { DataTypes } = require('sequelize');
const { database } = require('../../../../utils/index');

const CourseSections = database.define('CourseSections', {
	id: {
		type: DataTypes.UUID,
		primaryKey: true,
		allowNull: false,
		defaultValue: DataTypes.UUIDV4,
	},
});

module.exports = CourseSections;
