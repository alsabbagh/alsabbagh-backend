const {
	addTest,
	updateTest,
	deleteTest,
	getAllTests,
	updateMark,
	deleteTestUser,
	getCourseTests,
	getCourseStudentTests,
	getMarks,
	removeTestUser,
	getUserMarks,
} = require('./handler');
const router = require('express').Router();

router.post('/addTest', addTest);

router.put('/update', updateTest);

router.delete('/deleteTest/:testId', deleteTest);

router.get('/', getAllTests);

router.get('/marks', getMarks);

router.get('/user', getUserMarks);

router.delete('/mark/:id', removeTestUser);

router.get('/getCoursetests/:id', getCourseTests);

router.get('/getCourseStudentTests/:courseId/:userId?', getCourseStudentTests);

router.put('/updateMarks', updateMark);

module.exports = router;
