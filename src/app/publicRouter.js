const router = require('express').Router();

/*******************
 * @Router /public *
 *******************/

router.use('/users', require('./users/publicRouter'));

router.use('/classes', require('./classes/publicRouter'));

router.use('/server', require('./server/publicRouter'));

module.exports = router;
