const { DataTypes } = require('sequelize');
const { user } = require('../../users/models/enum.json');
const { database } = require('../../../../utils/index');

const Notification = database.define('Notification', {
	id: {
		type: DataTypes.UUID,
		primaryKey: true,
		allowNull: false,
		defaultValue: DataTypes.UUIDV4,
	},
	title: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	body: {
		type: DataTypes.TEXT,
		allowNull: false,
	},
	sectionId: {
		type: DataTypes.UUID,
		allowNull: true,
		defaultValue: null,
	},
	isRead: {
		type: DataTypes.BOOLEAN,
		defaultValue: false,
	},
	type: {
		type: DataTypes.ENUM,
		values: user,
		defaultValue: user[1],
	},
});

module.exports = Notification;
