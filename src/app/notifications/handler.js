const { constants } = require('../../../utils');
const Notification = require('./service');

module.exports = {
	add: async (notification, users) => {
		await new Notification(notification).add(users);
		return 'done';
	},

	getNew: async (req, res) => {
		if (req.user) {
			const id = req.user.id;
			const result = await Notification.getNew(id);
			res.status(result.code).send({
				data: result.data,
			});
		}
	},

	unreadNotifications: async (req, res) => {
		const id = req.user.id;
		const result = await Notification.unreadNotifications(id);
		res.status(result.code).send({
			data: result.data,
		});
	},

	getAll: async (req, res) => {
		const id = req.user.id;
		const { limit } = constants;
		const pageNumber = req.query.pageNumber - 1 || 0;
		const skip = pageNumber * limit;

		const result = await Notification.getAll(id, limit, skip);
		res.status(result.code).send({
			data: result.data,
		});
	},

	update: async (req, res) => {
		const id = req.user.id;
		const result = await Notification.update(id);
		res.status(result.code).send({
			data: result.data,
		});
	},

	addToStudents: async (req, res) => {
		const users = req.body.users;
		const notification = {
			title: req.body.title,
			body: req.body.body,
		};
		const result = await new Notification(notification).add(users);
		res.status(result.code).send({
			data: 'done',
		});
	},

	addToSection: async (req, res) => {
		const { title, body, sectionId } = req.body;
		const notification = {
			title,
			body,
			sectionId,
		};
		const result = await new Notification(notification).addToSection();
		res.status(result.code).send({
			data: result.data,
		});
	},

	addToClass: async (req, res) => {
		const { title, body, classId } = req.body;
		const notification = {
			title,
			body,
		};
		const result = await new Notification(notification).addToClass(classId);
		res.status(result.code).send({
			data: result.data,
		});
	},
};
