const { Op } = require('sequelize');
const { httpStatus } = require('../../../utils/index');

const NotificationModel = require('./models/Notification');
const UserModel = require('../users/models/User');
const UserSectionsModel = require('../users/models/UserSections');
const { user: userTypes } = require('../users/models/enum.json');

class Notification {
	constructor(data) {
		this.title = data.title;
		this.body = data.body;
		this.sectionId = data.sectionId;
		this.type = data.type;
	}

	async add(users) {
		try {
			const nots = users.map((user) => {
				const obj = {
					title: this.title,
					body: this.body,
					sectionId: this.sectionId,
					type: this.type,
					userId: user.userId || user.id,
				};
				return obj;
			});
			const result = await NotificationModel.bulkCreate(nots);
			return {
				data: result,
				code: httpStatus.CREATED,
			};
		} catch (error) {
			console.error(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	//! add notification to section
	async addToSection() {
		const users = await UserSectionsModel.findAll({
			where: {
				sectionId: { [Op.like]: `%${this.sectionId}%` },
			},
			attributes: ['userId'],
		});
		return await this.add(users);
	}

	//! add notification to class
	async addToClass(classId) {
		const users = await UserModel.findAll({
			where: {
				classId: { [Op.like]: `%${classId}%` },
			},
			attributes: ['id'],
		});
		return await this.add(users);
	}

	static async getNew(id) {
		try {
			const result = await NotificationModel.findAll({
				where: {
					userId: id,
					isNotify: false,
				},
			});
			const username = await UserModel.findOne({
				where: {
					id: id,
				},
				attributes: ['username'],
			});
			await NotificationModel.update(
				{
					isNotify: true,
				},
				{
					where: {
						userId: id,
						isNotify: false,
					},
				}
			);
			return {
				data: 'done',
				code: httpStatus.OK,
			};
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async unreadNotifications(id) {
		try {
			const result = await NotificationModel.findAll({
				where: {
					userId: id,
					isRead: false,
				},
			});
			return {
				data: result.length,
				code: httpStatus.OK,
			};
		} catch (error) {
			console.log(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async getAll(id, limit, skip) {
		try {
			const user = await UserModel.findByPk(id, { attributes: ['type'] });

			let condition = {};

			if (user && user.type === userTypes[0]) {
				condition = {
					type: userTypes[0],
				};
			}

			if (user && user.type === userTypes[1]) {
				condition = {
					userId: id,
				};
			}

			const result = await NotificationModel.findAndCountAll({
				where: condition,
				order: [['createdAt', 'DESC']],
				limit,
				offset: skip,
			});
			return {
				data: result,
				code: httpStatus.OK,
			};
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async update(id) {
		try {
			const user = await UserModel.findByPk(id, { attributes: ['type'] });

			let condition = {};

			if (user && user.type === userTypes[0]) {
				condition = {
					type: userTypes[0],
					isRead: false,
				};
			}

			if (user && user.type === userTypes[1]) {
				condition = {
					userId: id,
					isRead: false,
				};
			}

			await NotificationModel.update(
				{
					isRead: true,
				},
				{
					where: condition,
				}
			);
			return {
				data: 'updated',
				code: httpStatus.OK,
			};
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}
}

module.exports = Notification;
