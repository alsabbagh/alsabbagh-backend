const { getAll } = require('./handler');
const router = require('express').Router();

/*************************
 * @Router /api/Chart *
 *************************/


router.get('/', getAll);

module.exports = router;
