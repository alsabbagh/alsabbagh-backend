const Chart = require('./service');

module.exports = {
	getAll: async (req, res) => {
		const result = await Chart.get();
		res.status(result.code).send({
			data: result.data,
		});
	},
};
