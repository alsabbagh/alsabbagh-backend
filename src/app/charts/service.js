const { fn, col } = require('sequelize');

const SectionModel = require('../sections/models/Section');
const CourseModel = require('../courses/models/Course');
const TestModel = require('../activities/models/Test');
const UserSectionsModel = require('../users/models/UserSections');
const ClassModel = require('../classes/models/Class');

const { httpStatus } = require('../../../utils');

class Chart {
	static async get() {
		try {
			const result = await UserSectionsModel.findAll({
				attributes: {
					include: [[fn('COUNT', col('UserSections.userId')), 'students']],
				},
				include: {
					model: SectionModel,
					as: 'section',
					attributes: ['title'],
					include: {
						model: CourseModel,
						as: 'course',
						attributes: {
							exclude: ['id', 'title', 'createdAt', 'updatedAt'],
						},
						include: {
							model: ClassModel,
							as: 'class',
							attributes: {
								exclude: ['id', 'createdAt', 'updatedAt', 'isActive'],
							},
						},
					},
				},
				group: ['UserSections.sectionId'],
			});

			const result2 = await TestModel.findAll({
				attributes: {
					include: [[fn('COUNT', col('Test.id')), 'tests']],
				},

				include: {
					model: SectionModel,
					as: 'section',
					attributes: ['title'],
					include: {
						model: CourseModel,
						as: 'course',
						attributes: {
							exclude: ['id', 'title', 'createdAt', 'updatedAt'],
						},
						include: {
							model: ClassModel,
							as: 'class',
							attributes: {
								exclude: ['id', 'createdAt', 'updatedAt', 'isActive'],
							},
						},
					},
				},
				group: ['Test.sectionId'],
			});

			return {
				data: {
					usersSection: result,
					testsSection: result2,
				},
				code: 200,
			};
		} catch (error) {
			console.log(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}
}

module.exports = Chart;
