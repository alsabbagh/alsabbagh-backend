const _ = require('lodash');
const { User, UserSection } = require('./service');
const { constants } = require('../../../utils/index');

module.exports = {
	register: async (req, res) => {
		const data = req.body;
		const result = await new User(data).register();
		res.status(result.code).send({
			data: result.data,
			token: result.token,
		});
	},

	login: async (req, res) => {
		const username = req.body.username;
		const password = req.body.password;
		const isAdmin = req.body.admin;
		const result = await User.login(username, password, isAdmin);
		res.status(result.code).send({
			data: result.data,
			token: result.token,
		});
	},

	addUser: async (req, res) => {
		const data = req.body;
		const result = await new User(data).addUser();
		res.status(result.code).send({
			data: result.data,
		});
	},

	update: async (req, res) => {
		const id = req.body.id || req.user.id;
		const data = req.body;
		const result = await new User(data).update(id);
		res.status(result.code).send({
			data: result.data,
		});
	},

	updateActivation: async (req, res) => {
		const id = req.body.studentId;
		const isActive = req.body.status;
		const result = await User.updateActivation(id, isActive);
		res.status(result.code).send({
			data: result.data,
		});
	},

	deleteUser: async (req, res) => {
		const id = req.params.id;
		const result = await User.delete(id);
		res.status(result.code).send({
			data: result.data,
		});
	},

	getAll: async (req, res) => {
		const { limit } = constants;
		const pageNumber = req.query.pageNumber - 1 || 0;
		const skip = pageNumber * limit;
		const name = req.query.name ? req.query.name.trim() : '';
		const classId = req.query.classId || '';
		const sectionId = req.query.sectionId || '';
		const result = await User.get(limit, skip, name, classId, sectionId);
		res.status(result.code).send({
			data: result.data,
		});
	},

	updatePassword: async (req, res) => {
		const id = req.user.id;
		const oldPassword = req.body.oldPassword;
		const newPassword = req.body.newPassword;
		const result = await User.updatePassword(id, oldPassword, newPassword);
		res.status(result.code).send({
			data: result.data,
		});
	},

	addUserSection: async (req, res) => {
		let data = req.body;
		const id = req.user.id;

		if (!data.userId) {
			data = {
				...data,
				userId: id,
			};
		}

		const result = await new UserSection(data).add();
		res.status(result.code).send({
			data: result.data,
		});
	},

	getUserSections: async (req, res) => {
		const id = req.params.id || req.user.id || '';
		const isAccept = req.query.isAccept;
		const result = await UserSection.getSections(id, isAccept);
		res.status(result.code).send({
			data: result.data,
		});
	},

	deleteUserSection: async (req, res) => {
		const id = req.params.id;
		const result = await UserSection.delete(id);
		res.status(result.code).send({
			data: result.data,
		});
	},

	getProgram: async (req, res) => {
		const id = req.user.id;

		const result = await UserSection.getProgram(id);

		res.status(result.code).send({
			data: result.data,
		});
	},
};
