const handler = require('./handler');
const router = require('express').Router();
/****************************
 * @Router /public/user *
 ****************************/

router.post('/register', handler.register);

router.post('/login', handler.login);

module.exports = router;
