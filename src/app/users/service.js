const { Op } = require('sequelize');
const { httpStatus } = require('../../../utils/index');
const UserModel = require('./models/User');
const UserSections = require('./models/UserSections');
const ClassModel = require('../classes/models/Class');
const SectionModel = require('../sections/models/Section');
const CourseModel = require('../courses/models/Course');

const { add: addNotification } = require('../notifications/handler');
const { user } = require('./models/enum.json');

const days = {
	السبت: 'السبت',
	الأحد: 'الأحد',
	الاحد: 'الأحد',
	الإحد: 'الأحد',
	الاثنين: 'الإثنين',
	الأثنين: 'الإثنين',
	الإثنين: 'الإثنين',
	الثلاثاء: 'الثلاثاء',
	الاربعاء: 'الاربعاء',
	الأربعاء: 'الاربعاء',
	الإربعاء: 'الاربعاء',
	الخميس: 'الخميس',
	الجمعة: 'الجمعة',
};
class User {
	constructor(data) {
		this.username = data.username;
		this.name = data.name;
		this.password = data.password;
		this.mobilePhone = data.mobilePhone;
		this.fatherPhone = data.fatherPhone;
		this.motherPhone = data.motherPhone;
		this.phone = data.phone;
		this.type = data.type;
		this.classId = data.classId;
		this.gender = data.gender;
	}

	async register() {
		try {
			const result = await UserModel.create(this);
			const token = await result.generateVerificationToken();
			return {
				data: result,
				token: token,
				code: httpStatus.CREATED,
			};
		} catch (error) {
			console.error(error.message);
			if (error.message.includes('email'))
				return {
					data: 'email already existed, please login',
					code: httpStatus.ALREADY_REGISTERED,
				};
			else if (error.message.includes('username'))
				return {
					data: 'username already existed, please choose another one',
					code: httpStatus.ALREADY_REGISTERED,
				};
			else
				return {
					data: error.message,
					code: httpStatus.ALREADY_REGISTERED,
				};
		}
	}

	async addUser() {
		try {
			const result = await UserModel.create(this);
			return {
				data: result,
				code: httpStatus.CREATED,
			};
		} catch (error) {
			console.error(error.message);
			if (error.message.includes('email'))
				return {
					data: 'email already existed, please login',
					code: httpStatus.ALREADY_REGISTERED,
				};
			else if (error.message.includes('username'))
				return {
					data: 'username already existed, please choose another one',
					code: httpStatus.ALREADY_REGISTERED,
				};
			else
				return {
					data: error.message,
					code: httpStatus.ALREADY_REGISTERED,
				};
		}
	}

	static async login(username, password, isAdmin) {
		try {
			const result = await UserModel.findOne({
				where: {
					username: username,
				},
			});
			if (result && result.type === user[1] && isAdmin) {
				return {
					data: 'student type cannot login',
					code: httpStatus.FORBIDDEN,
				};
			}
			if (result && (await result.comparePassword(password))) {
				const token = await result.generateVerificationToken();
				return {
					data: result,
					token: token,
					code: httpStatus.OK,
				};
			} else {
				return {
					data: 'username or password is not correct',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async get(limit, skip, name, classId, sectionId) {
		try {
			const result = await UserModel.findAndCountAll({
				order: [['name', 'ASC']],
				limit,
				offset: skip,
				where: {
					type: { [Op.not]: 'manager' },
					name: { [Op.like]: `%${name}%` },
					classId: { [Op.like]: `%${classId}%` },
				},
				include: [
					{
						model: ClassModel,
						attributes: ['title'],
						as: 'class',
					},
					{
						model: SectionModel,
						as: 'section',
						where: {
							id: { [Op.like]: `%${sectionId}%` },
						},
						required: sectionId ? true : false,
					},
				],
				distinct: true,
			});
			return {
				data: result,
				code: httpStatus.OK,
			};
		} catch (error) {
			console.info(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	async update(id) {
		try {
			const result = await UserModel.update(this, {
				where: {
					id: id,
				},
			});
			if (result[0] === 1) {
				return {
					data: 'updated successfully',
					code: httpStatus.UPDATED,
				};
			} else {
				return {
					data: 'something wrong happened',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async updateActivation(id, isActive) {
		try {
			const result = await UserModel.update(
				{
					isActive: isActive,
				},
				{
					where: {
						id: id,
					},
				}
			);
			if (result[0] === 1) {
				return {
					data: `updated`,
					code: httpStatus.UPDATED,
				};
			} else {
				return {
					data: 'something wrong happened',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async delete(id) {
		try {
			const result = await UserModel.destroy({
				where: {
					id: id,
				},
			});
			if (result) {
				return {
					data: 'deleted successfully',
					code: httpStatus.DELETED,
				};
			} else {
				return {
					data: 'something wrong happened',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async sendMail(email) {
		try {
			const code = sendMail(email);
			if (code !== undefined) {
				const result = await UserModel.update(
					{
						verificationCode: code,
					},
					{
						where: {
							email: email,
						},
					}
				);
				if (result[0] === 1) {
					return {
						data: 'we sent a verification code to your email, please check it',
						code: httpStatus.OK,
					};
				} else {
					return {
						data: 'something wrong happened',
						code: httpStatus.BAD_REQUEST,
					};
				}
			} else {
				return {
					data: 'something wrong happened',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async checkCode(email, code) {
		try {
			const result = await UserModel.findOne({
				where: {
					email: email,
					verificationCode: code,
				},
			});
			if (result) {
				return {
					data: 'done',
					code: httpStatus.OK,
				};
			} else {
				return {
					data: 'please check your code',
					code: httpStatus.NOT_FOUND,
				};
			}
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async resetPassword(password, email) {
		try {
			const result = await UserModel.update(
				{
					password: password,
				},
				{
					where: {
						email: email,
					},
				}
			);
			if (result[0] == 1) {
				return {
					data: 'updated',
					code: httpStatus.UPDATED,
				};
			} else {
				return {
					data: 'something wrong happened',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async updatePassword(id, oldPassword, newPassword) {
		try {
			const result = await UserModel.findOne({
				where: {
					id: id,
				},
			});
			if (result && (await result.comparePassword(oldPassword))) {
				const user = await UserModel.update(
					{
						password: newPassword,
					},
					{
						where: {
							id: id,
						},
					}
				);
				if (user[0] == 1) {
					return {
						data: 'updated successfully',
						code: httpStatus.UPDATED,
					};
				} else {
					return {
						data: 'something wrong happened',
						code: httpStatus.BAD_REQUEST,
					};
				}
			} else {
				return {
					data: 'password is not correct',
					code: httpStatus.VALIDATION_ERROR,
				};
			}
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}
}

class UserSection {
	constructor(data) {
		this.sectionId = data.sectionId;
		this.userId = data.userId;
		this.isAccept = data.isAccept || true;
	}

	async add() {
		try {
			const section = await SectionModel.findByPk(this.sectionId);
			const usersSection = await UserSections.count({
				where: {
					sectionId: this.sectionId,
				},
			});

			if (+section.maxQuantity === +usersSection) {
				return {
					data: 'الشعبة ممتلئة',
					code: httpStatus.VALIDATION_ERROR,
				};
			}

			if (section.maxQuantity <= usersSection + 1) {
				await SectionModel.update(
					{ isActive: false },
					{
						where: {
							id: this.sectionId,
						},
					}
				);
			}

			const result = await UserSections.create(this);
			const _user = await UserModel.findByPk(this.userId);

			if (_user && _user.type === user[1]) {
				addNotification(
					{
						title: `تمت إضافة الطالب ${_user.name} إلى الشعبة ${section.title}`,
						body: '',
						sectionId: this.sectionId,
						type: user[0],
					},
					[{ userId: _user.id }]
				);
			}

			return {
				data: result,
				code: httpStatus.CREATED,
			};
		} catch (error) {
			if (error.message.toLowerCase().includes('validation error')) {
				return {
					data: error.message,
					code: httpStatus.BAD_REQUEST,
				};
			}
			return {
				data: error.message,
				code: httpStatus.INTERNAL_SERVER_ERROR,
			};
		}
	}

	static async update(id) {
		try {
			const result = await UserSections.update(
				{ isAccept: true },
				{
					where: {
						id,
					},
				}
			);
			if (result[0] == 1) {
				return {
					data: 'updated',
					code: httpStatus.UPDATED,
				};
			} else {
				return {
					data: 'something wrong happened',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async getSections(id, isAccept) {
		const where =
			Boolean(isAccept) === true || isAccept === undefined
				? {
						isAccept: true,
						userId: id,
				  }
				: Boolean(isAccept) === false && {
						isAccept: false,
				  };

		try {
			const result = await UserSections.findAll({
				where,
				attributes: ['id'],
				include: [
					{
						model: SectionModel,
						as: 'section',
						attributes: ['title', 'id', 'description'],
						include: {
							model: CourseModel,
							as: 'course',
							attributes: ['title'],
						},
					},
					{ model: UserModel, as: 'user', attributes: ['id', 'name'] },
				],
			});

			return {
				data: result,
				code: httpStatus.OK,
			};
		} catch (error) {
			console.log(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async delete(id) {
		try {
			const res = await UserSections.findByPk(id, {
				attributes: ['sectionId'],
			});

			const result = await UserSections.destroy({
				where: {
					id,
				},
			});

			if (result) {
				const section = await SectionModel.findByPk(res.sectionId);

				const usersSection = await UserSections.count({
					where: {
						sectionId: res.sectionId,
					},
				});

				if (section && +section.maxQuantity > +usersSection) {
					await SectionModel.update(
						{
							isActive: true,
						},
						{
							where: {
								id: res.sectionId,
							},
						}
					);
				}

				return {
					data: 'deleted!',
					code: httpStatus.DELETED,
				};
			}

			return {
				data: 'تم الحذف!',
				code: httpStatus.DELETED,
			};
		} catch (error) {
			console.log(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async getProgram(id) {
		try {
			const userSections = await UserSections.findAll({
				where: {
					userId: id,
				},
				attributes: ['sectionId'],
			});

			const sectionsIds = userSections.map((section) => section.sectionId);

			const sections = await SectionModel.findAll({
				where: {
					id: { [Op.in]: sectionsIds },
				},
			});

			const program = [];

			sections.forEach((section) => {
				const _days = section.description.split('.');

				_days.forEach((day) => {
					if (day) {
						const date = day.split(':');
						const _dates = {
							day: days[date[0].trim()],
							time: date[1].trim(),
							section: section.title,
						};

						program.push(_dates);
					}
				});
			});

			return {
				data: program,
				code: httpStatus.OK,
			};
		} catch (error) {
			console.log(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}
}

module.exports = { User, UserSection };
