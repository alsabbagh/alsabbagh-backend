const {
	update,
	deleteUser,
	getAll,
	updatePassword,
	addUser,
	addUserSection,
	getUserSections,
	deleteUserSection,
	getProgram
} = require('./handler');
const router = require('express').Router();

/*************************
 * @Router /api/user *
 *************************/

router.post('/', addUser);

router.post('/sections', addUserSection);

router.get('/sections/:id?', getUserSections);

router.delete('/sections/:id', deleteUserSection);

router.put('/update', update);

router.delete('/delete/:id', deleteUser);

router.get('/', getAll);

router.get('/program', getProgram);

router.put('/updatePassword', updatePassword);

module.exports = router;
