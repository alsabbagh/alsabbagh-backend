const { DataTypes } = require('sequelize');
const { database } = require('../../../../utils/index');

const UserSections = database.define('UserSections', {
	id: {
		type: DataTypes.UUID,
		primaryKey: true,
		allowNull: false,
		defaultValue: DataTypes.UUIDV4,
	},
	isAccept: {
		type: DataTypes.BOOLEAN,
		defaultValue: false,
	},
});

module.exports = UserSections;
