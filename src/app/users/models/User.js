const { DataTypes } = require('sequelize');
const { database } = require('../../../../utils/index');
const { gender, user } = require('./enum.json');
const bcrypt = require('bcrypt');
const { SALT_WORK_FACTOR, key, cipherAlgorithm } = require('../../../../config/index');
const jwt = require('jsonwebtoken');

const User = database.define(
	'User',
	{
		id: {
			type: DataTypes.UUID,
			primaryKey: true,
			allowNull: false,
			defaultValue: DataTypes.UUIDV4,
		},
		username: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		password: {
			type: DataTypes.STRING,
			allowNull: false,
			trim: true,
		},
		name: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		mobilePhone: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		fatherPhone: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		motherPhone: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		phone: {
			type: DataTypes.STRING,
		},
		gender: {
			type: DataTypes.ENUM,
			values: gender,
			defaultValue: gender[0],
		},
		type: {
			type: DataTypes.ENUM,
			values: user,
			defaultValue: user[1],
		},
		isActive: {
			type: DataTypes.BOOLEAN,
			defaultValue: true,
		},
	},
	{
		indexes: [
			{
				unique: true,
				fields: ['username'],
			},
		],
	}
);

const hashPassword = async function (user) {
	try {
		const password = user.password || user.attributes.password;
		if (password !== undefined) {
			if (user.password === undefined) {
				user.attributes.password = await bcrypt.hash(password, SALT_WORK_FACTOR);
			} else {
				user.password = await bcrypt.hash(password, SALT_WORK_FACTOR);
			}
			return user;
		}
		return null;
	} catch (error) {
		console.log(error.message);
		return null;
	}
};

User.prototype.comparePassword = async function (password) {
	return await bcrypt.compare(password, this.password);
};

User.prototype.generateVerificationToken = async function () {
	return jwt.sign({ id: this.id }, key, {
		algorithm: cipherAlgorithm,
	});
};

User.beforeCreate(hashPassword);
User.beforeUpdate(hashPassword);
User.beforeBulkUpdate(hashPassword);

User.prototype.toJSON = function () {
	let user = Object.assign({}, this.get());
	delete user.password;
	delete user.createdAt;
	delete user.updatedAt;
	delete user.verificationCode;
	return user;
};

module.exports = User;
