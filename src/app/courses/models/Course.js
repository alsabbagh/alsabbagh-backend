const { DataTypes } = require('sequelize');
const { database } = require('../../../../utils/index');

const Course = database.define(
	'Course',
	{
		id: {
			type: DataTypes.UUID,
			primaryKey: true,
			allowNull: false,
			defaultValue: DataTypes.UUIDV4,
		},
		title: {
			type: DataTypes.STRING,
			allowNull: false,
		},
	}
);

module.exports = Course;
