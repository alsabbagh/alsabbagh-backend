const { httpStatus } = require('../../../utils/index');
const { Op } = require('sequelize');

const CourseModel = require('./models/Course');
const ClassModel = require('../classes/models/Class');
const InstructorModel = require('../instructors/models/Instructor');

class Course {
	constructor(data) {
		this.title = data.title;
		this.instructorId = data.instructorId;
		this.classId = data.classId;
	}

	async add() {
		try {
			const result = await CourseModel.create(this);
			return {
				data: result,
				code: httpStatus.CREATED,
			};
		} catch (error) {
			console.error(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	async update(id) {
		try {
			const result = await CourseModel.update(this, {
				where: {
					id: id,
				},
			});
			if (result[0] == 1) {
				return {
					data: 'updated',
					code: httpStatus.UPDATED,
				};
			} else {
				console.info(result);
				return {
					data: 'something wrong happened',
					code: httpStatus.BAD_REQUEST,
				};
			}
		} catch (error) {
			console.info(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async delete(id) {
		try {
			await CourseModel.destroy({
				where: {
					id: id,
				},
			});
			return {
				data: 'deleted',
				code: httpStatus.DELETED,
			};
		} catch (error) {
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}

	static async get(classId) {
		try {
			const result = await CourseModel.findAll({
				attributes: ['id', 'title'],
				include: [
					{
						model: ClassModel,
						as: 'class',
						where: {
							id: { [Op.like]: `%${classId}%` },
						},
						attributes: ['id', 'title'],
					},
					{
						model: InstructorModel,
						as: 'instructor',
						attributes: ['id', 'name'],
					},
				],
			});
			return {
				data: result,
				code: httpStatus.OK,
			};
		} catch (error) {
			console.info(error);
			return {
				data: error.message,
				code: httpStatus.BAD_REQUEST,
			};
		}
	}
}

module.exports = Course;
