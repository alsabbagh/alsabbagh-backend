module.exports = {
	port: 4000,
	cipherAlgorithm: 'HS256',
	SALT_WORK_FACTOR: 8,
	key: 'TOP_SECRET',
};
