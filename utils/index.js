process.env.TZ = 'UTC';

module.exports = {
	httpStatus: require('./constants/httpStatus'),
	database: require('./database/config'),
	passport: require('./authentication'),
	common: require('./common/functions'),
	constants: require('./common/constants'),
	fileUploader: require('./multer/single-file'),
};
