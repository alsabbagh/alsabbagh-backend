var dateFormat = require('dateformat');

const formatDate = (date) => {
	return dateFormat(date, 'dd/mm/yyyy');
};

const formateTime = (date) => {
	return dateFormat(date, 'h:MM TT');
};

const formatDateTime = (date) => {
	return dateFormat(date, 'dd/mm/yyyy h:MM TT');
};

module.exports = {
	formatDate,
	formateTime,
	formatDateTime,
};
