const limit = 50;

const appVersion = '2.1.0';

module.exports = { limit, appVersion };
