const passportJWT = require('passport');
const httpStatus = require('../constants/httpStatus');

module.exports = {
	authenticate: (req, res, done) => {
		passportJWT.authenticate('jwt', { session: false }, (id) => {
			if (id) {
				req.user = id;
				return done();
			}
			res.status(httpStatus.UNAUTHORIZED).send({
				data: 'Unauthorized',
			});
		})(req, res);
	},
};
