const Sequelize = require('sequelize').Sequelize;

const sequelize = new Sequelize('alsabbagh_v3', 'root', '', {
	dialect: 'mysql',
	host: '127.0.0.1',
	logging: false,
});

// const sequelize = new Sequelize('alsabbagh_v3', 'taha', 'P@ssw0rd', {
// 	dialect: 'mysql',
// 	host: '127.0.0.1',
// 	logging: false,
// });

// const sequelize = new Sequelize('atconcept_alsabbagh', 'atconcept_tahakassar', 'taha.1234', {
// 	dialect: 'mysql',
// 	host: '198.54.114.174',
// 	logging: false,
// 	define: {
// 		timestamps: false,
// 	},
// });

module.exports = sequelize;
